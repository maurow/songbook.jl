#!/usr/bin/env julia
#
# Assumes this is run in the directory of the book project.  Takes the
# dirname as project-name

# A few settings
# TODO: make into command-line options
# Which ogg to play when scanning to on-button:
#   - hello: synthetic speak hello
#   - s001: play song #1, move all songs back by 1
#   - s000: play song #0, this song must be added manually to "media/"
welcome_song = ["hello", "s001", "s000"][1]

# print options
circ = 24.5 # size of the circle in the box
#circ = 15.5 # size of the circle in the box
box = circ*1.08 # size of box in mm
ress = [600,1200][2:2] # DPI resolutions.
fontsize = 0.5 # size of the font of the number in each box

# Extract name of current directory
tmp = splitdir(pwd())[2]
ind = findlast(tmp, '-')
proj_name = tmp[1:ind-1]
product_id = 0
try
    product_id = parse(Int,tmp[ind+1:end])
end
if !(0<product_id<1000)
    println("Directory should be of form project-name-785 where 785 if the product-id")
    error("product-id (last part of directory name) needs 0<product_id<1000)")
end
normalize_volume = "-30dBFS" # change the volume of the .wavs
n_copies = 1 # number of copies of OIDs-pics to put into the document.

# some switches for the longer tasks
transcode = [true, false][1]
make_pngs = [true, false][1]
make_pdf = [true, false][1]

###
# Global settings, adjust if necessary:
include("SongBook.jl")
homedir = dirname(functionloc(transcode2ogg, (Any,Any))[1]) # gets dir of SongBook.jl
const tttool = joinpath(homedir, "../tip-toi-reveng/tttool") # path to the tttool executable

##############
# Start (probably nothing to edit after this...)
##############

# rip
if !isdir("rip") || length(readdir("rip"))==0
    println("Move/rip .wav files to dir rip/")
    println("")
    println("Want to rip a CD now? (y/n)")
    ans = readline(STDIN)
    if ans=="y"
        print("Ripping CD...")
        mkdir_t("rip")
        cd("rip")
        run(`cdparanoia -B`)
        cd("..")
        println("done")
    end
end

if normalize_volume!=false
    print("Normalizing volume...")
    cd("rip")
    fls = filter(r".*\.wav", readdir())
    run(`normalize -b -a$normalize_volume $(fls...)`)
    cd("..")
    println("done")
end

# transcode to ogg
print("Transcoding wav->ogg...")
n_songs = transcode2ogg("rip", "media", overwirte=transcode)
println("done")

# write yaml file
print("Writing .yaml and assembling .gme...")
yaml_base = replace(yaml_template, "!prodid!", product_id)
yaml_base = replace(yaml_base, "!proj!", proj_name)
yaml_base = replace(yaml_base, "!hello!", welcome_song)
start = welcome_song=="s001" ? 2 : 1
yaml = yaml_base * generate_yaml(n_songs, start=start)
if welcome_song=="hello" # this would not work together with speakyes=true
    yaml *="speak:\n  hello: \"hello\""
end
open(proj_name*".yaml", "w") do io
    write(io, yaml)
end
yaml = yaml_base * generate_yaml(n_songs, start=start, speakyes=true)
open(proj_name*"-debug.yaml", "w") do io
    write(io, yaml)
end

# generate gme
run(`$tttool assemble $(proj_name*".yaml")`)
run(`$tttool assemble $(proj_name*"-debug.yaml")`)
println("done")

# create OID pngs
for res in ress
    printdir = "print-$(res)dpi"
    mkdir_t(printdir)
    println("Creating printing sheet for resolution: $res dpi")
    # power symbol
    cp(homedir*"/power_symbol.png", printdir*"/power_symbol.png", remove_destination=true)
    # make pngs
    if make_pngs
        run(`$tttool oid-code -d $res $(proj_name*".yaml")`)
    end

    if make_pdf
        # crop them
        if res==600
            pngpix = 2400 # as from tttool
        elseif res==1200
            pngpix = 4800 # as from tttool
        else
            error("parameter res needs to be 600dpi or 1200dpi")
        end
        pngsize = pngpix/res*25.4
        pix = round(Int,box/25.4*res)
        println("Cropping pngs")
        for f in filter(r".*\.png",readdir())
            print(".")
            run(`convert $f -crop $(pix)x$(pix)+0+0 $f`)
            # move it
            mv(f, printdir*"/$f", remove_destination=true)
        end
        println("done")

        # write tex file:
        tex = make_latex(printdir, proj_name, prod_id=product_id, box=box, circ=circ, res=res, fsz=fontsize,
                         copies=n_copies)
        open(printdir*"/"*proj_name*".tex", "w") do io
            write(io, tex)
        end

        # make pdf:
        cd(printdir)
        run(`pdflatex $proj_name.tex`)
        cd("..")
    end
end

println("Done\n")
println("Now copy *.gme file to tiptoi")
