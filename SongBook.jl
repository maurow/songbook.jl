# This tool helps creating song-books to be used with the
# [Tiptoi](https://www.tiptoi.com/) pen.  The idea is to use
# pre-existing song books which come with a CD and add Tiptoi labels to
# each song in the book.  When Tiptoi'ed the song will play.  So, it's
# kinda an MP3 player.

"Raw strings with no interpolation"
macro raw_str(s) s end

"makes dir if not exists"
mkdir_t(dir) =  !isdir(dir) ? mkdir(dir) : nothing

"Template for the yaml file.  Strings starting with `!` will be replaced."
const yaml_template =
raw"""
# This is the template file with which to make song books.

# You can compile this running
# ./tttool assemble song-book.yaml song-book.gme

# The product ID must match that of the book you want to use this with
product-id: !prodid!  # better 900+

# The media path is optional
# The default is "%s", i.e. to look in the current directory
# If you use "example/Buch1_%s", here then P(test) will use
# the file "example/Buch1_test.ogg".
# Ogg files should be Mono, 22050 Hz.
# tttool will append .wav or .ogg and uses whatever it finds
media-path: media/%s

# The comment does not really matter
comment: !proj!

# Register initalisation. This is simply list of set-commands.
# Every register not set is initialized to 0
init: $mode:=1

# The sounds to play on start-up
welcome: !hello!

# This is the interesting part: Scripts.
# Technically, this is an assoiative list with the OID as an index,
# and a list of strings as the content
# scripts:
#   # We begin to specify what to do when the area in the book with the OID code
#   # s001 is touched.
#   s001:
#     - P(s001) # play song s001
#   s002:
#     - P(s002)
# speak:
#   hallo: "hallo"
#   s001: "song 1"
#   s002: "song 2"
"""

"Creates the yaml code for the songs"
function generate_yaml(n_songs; start=1, speakyes=false)
    io = IOBuffer()
    println(io, "scripts:")
    for i=start:n_songs
        ii = @sprintf("%03i", i)
        println(io,"  s$ii:")
        println(io,"    - P(s$ii)")
    end
    if speakyes
        println(io,"speak:")
        println(io,"  hello: \"hello\"")
        for i=0:n_songs
            ii = @sprintf("%03i", i)
            println(io,"  s$ii: \"song $ii\"")
        end
    end
    return readall(seek(io,0))
end



"""
Transcodes .wav in indir to .ogg in outdir (both relative to curdir)

returns number of songs
"""
function transcode2ogg(indir, outdir; overwirte=false)
    pdir = pwd()
    cd(indir)
    outdir_ =
    fls = readdir()
    i = 1
    for f in fls
        if splitext(f)[2]!=".wav"
            continue
        end
        ii = @sprintf("%03i", i)
        out = joinpath(pdir, outdir, "s$ii.ogg")
        if !isfile(out) || overwirte
            run(`oggenc -b 32 --downmix --resample 22050 $f -o $out`)
        end
        i += 1
    end
    cd(pdir)
    return i-1
end


const documentwidth = 190  # A4 paper, also insert below
const tex_start =
raw"""
\documentclass[a4paper]{article}
\usepackage{graphicx}
\usepackage{rotating}

% A4 210 × 297 mm
\usepackage[width=190mm, height=275mm]{geometry}
\setlength{\unitlength}{1mm}
\setlength\fboxsep{0pt}
\setlength{\fboxrule}{1pt}
\begin{document}
\pagestyle{empty}
"""

# this has the following tags to be replaced:
#
# !box! -- size of box (mm)
# !halfbox! -- !box!/2 (mm)
# !trim! -- how much to cut off
# !circ! -- size of circle (6mm)
# !res! -- resolution in DPI
# !img! -- image file
# !fsz! -- fontsize scale
# !text! -- the text to be plotted
# const tex_block =
# raw"""
# \fbox{\begin{picture}(!box!, !box!)
# \put(!halfbox!,!halfbox!){\scalebox{6}[6]{\circle{!circ!}}}
# \put(0,0){\includegraphics[trim={0mm 0mm !trim!mm !trim!mm}, clip, resolution=!res!]{!img!}}
# \put(0,0){\scalebox{!fsz!}[!fsz!]{\textsf{!text!}}}
# \end{picture}}"""
# const tex_end =
# raw"""
# \end{document}
# """
const tex_block =
raw"""
\fbox{\begin{picture}(!box!, !box!)
\put(!halfbox!,!halfbox!){\scalebox{6}[6]{\circle{!circ!}}}
\put(0,0){\includegraphics[resolution=!res!]{!img!}}
\put(0.2,0.2){\scalebox{!fsz!}[!fsz!]{\textsf{!text!}}}
%\put(0.2,0.2){\rotatebox{90}{\scalebox{!fsz!}[!fsz!]{\textsf{!text!}}}} % rotated text
\end{picture}}"""
# \begin{rotate}{90}
# \scalebox{!fsz!}[!fsz!]{\textsf{!text!}}
# \end{rotate}"""

const tex_block_start =
raw"""
\fbox{\begin{picture}(!box!, !box!)
\put(0.0,!box2!){!header!}
\put(!halfbox!,!halfbox!){\scalebox{6}[6]{\circle{!circ!}}}
\put(!power_pos!,!power_pos!){\includegraphics[width=!power_sz!mm]{!power!}}
\put(0,0){\includegraphics[resolution=!res!]{!img!}}
\put(0.2,0.2){\scalebox{!fsz!}[!fsz!]{\textsf{!text!}}}
\end{picture}}"""

const tex_end =
raw"""
\end{document}
"""

function replace_in_tex_block(box, trim, circ, res, img, fsz, text; tex_str=tex_block)
    halfbox = box/2
    circ /= 6
    out = tex_str
    out = replace(out, "!box!", "$box")
    out = replace(out, "!halfbox!", "$(box/2)")
    out = replace(out, "!trim!", "$trim")
    out = replace(out, "!circ!", "$circ")
    out = replace(out, "!res!", "$res")
    out = replace(out, "!img!", img)
    out = replace(out, "!fsz!", "$fsz")
    out = replace(out, "!text!", text)
    return out
end

"""
Makes a string of the latex document containing all the OIDs
"""
function make_latex(dir, proj_name; prod_id=901, box=30, circ=20, res=1200, fsz=2, copies=1)
    pngwidth = 100 #4800/res*25.4 # pixels/dpi * mm-in-inches
    # files have names like: oid-901-s015.png
    ext = ".png"
    base = "oid-$prod_id-"

    trim = pngwidth-box

    pdir = pwd()
    cd(dir)
    io = IOBuffer()
    print(io, tex_start)
    boxes_per_row = div(documentwidth, box+0.5)-1

    for cop=1:copies
        start = replace_in_tex_block(box, trim, circ, res, base*"START"*ext, fsz, "Start", tex_str=tex_block_start)
        start = replace(start, "!header!", "\\small $proj_name \\quad \\quad product-id: $prod_id")
        start = replace(start, "!box2!", "$(box+3)")
        start = replace(start, "!power!", "power_symbol.png")
        start = replace(start, "!power_sz!", circ/2)
        start = replace(start, "!power_pos!", (box-circ)/2+circ/2-circ/2/2)
        println(io, start)
        for i=1:n_songs
            ii = @sprintf("%03i", i)
            println(io, replace_in_tex_block(box, trim, circ, res, base*"s$ii"*ext, fsz, ii))
            i+=1
            if mod(i,boxes_per_row)==0
                print(io,"\n\n")
            end
        end
        print(io,"\n \\vspace{5mm}\n")
    end
    print(io, tex_end)
    # end
    cd(pdir)
    return readall(seek(io,0))
end
