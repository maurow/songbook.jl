#!/usr/bin/env julia
#
# Input number of songs
#
# generates output like:
# scripts:
#   s001:
#     - P(s001) # play song s001
#   s002:
#     - P(s002)
# speak:
#   hallo: "hallo"
#   s001: "song 1"
#   s002: "song 2"

include("SongBook.jl")
print(generate_yaml(parse(ARGS[1]), speakyes=true))
