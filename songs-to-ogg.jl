#!/usr/bin/env julia
#
# Input: indir
#
# goes through all .wav in a folder and transcodes them ogg

include("SongBook.jl")

cd(ARGS[1])
if ~isdir("../media")
    mkdir("../media")
end

transcode2ogg(".", "../media")
