This tool helps creating song-books to be used with the
[Tiptoi](https://www.tiptoi.com/) pen.  The idea is to use
pre-existing song books which come with a CD and add Tiptoi labels to
each song in the book.  When Tiptoi'ed the song will play.  So, it's
kinda an MP3 player.

This is a command-line tool which probably only works on Linux.

The heavy lifting is done by the excellent Tiptoi tool
[http://tttool.entropia.de/](http://tttool.entropia.de/).


Manual:

- create a folder, named after the book
- to get songs, either:
    - insert CD into drive
    - or copy the `*.wav` in the `rip/` sub-folder
- run the script `create-book.jl` whilst in the folder named after the
  book.  It creates:
    - the main `*.gme` file and a `*-debug.gme`.  The first is the full
      thing, the latter contains synthesised speech for debugging.
      Note that you shouldn't have both on the Tiptoi at once as one
      will shadow the other.
    - two folders `print-600dpi` and  `print-1200dpi` which contain the
      sheets with the OID to be printed and stickered into the
      song-book.

Some options can be set by editing the first bit of the
`create-book.jl` script.

Note that printing is tricky, see
[http://tttool.entropia.de/](http://tttool.entropia.de/).  I'll try
and print it on
[see-though foil](http://www.folex.com/htm/580/en/CLP-ADHESIVE-P-CL-Home.htm?Article=47455&ReturnLink=579&Structure=16889)
which comes recommended by [http://mycvs.org/tagged/tiptoi](http://mycvs.org/tagged/tiptoi).

Dependencies:

- [Julia](http://julialang.org/) 0.4
- [http://tttool.entropia.de/](http://tttool.entropia.de/).
- cdparanoia (only for ripping CDs)
- LaTeX
- ImageMagick
- [normalize](http://normalize.nongnu.org/) (only for adjusting the volume)
